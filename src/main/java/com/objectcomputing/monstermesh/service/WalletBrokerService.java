package com.objectcomputing.monstermesh.service;

import com.objectcomputing.monstermesh.model.Wallets;

import java.util.Map;

public interface WalletBrokerService {
    Wallets retrieveWallets(Map<String, String> headers, String txnId, String userId);
}
