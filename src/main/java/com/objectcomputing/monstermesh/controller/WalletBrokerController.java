package com.objectcomputing.monstermesh.controller;

import com.objectcomputing.monstermesh.model.Wallets;
import com.objectcomputing.monstermesh.registry.UserCache;
import com.objectcomputing.monstermesh.service.IssuerRegistryService;
import com.objectcomputing.monstermesh.service.WalletBrokerService;
import io.opentracing.ActiveSpan;
import io.opentracing.SpanContext;
import io.opentracing.Tracer;
import io.opentracing.propagation.Format;
import io.opentracing.propagation.TextMapExtractAdapter;
import io.opentracing.tag.Tags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/broker")
public class WalletBrokerController {
    // TODO: make all log.info() log.debug()
    private static final Logger LOGGER = LoggerFactory.getLogger(WalletBrokerController.class);

    @Autowired
    private WalletBrokerService walletService;
    @Autowired
    private IssuerRegistryService registryService;
    @Autowired
    private UserCache userCache;
    @Autowired
    private Tracer tracer;

    @GetMapping("/{userId}/wallets")
    public Wallets retrieveUserWallets(@RequestHeader Map<String, String> headers, @PathVariable String userId) {
        LOGGER.info("Find wallets for: " + userId);
        LOGGER.info("Find wallets headers: " + headers);

        try (ActiveSpan span = startSpan(tracer, headers, Tags.SPAN_KIND_SERVER, "retrieveUserWallets")) {

            span.log(Collections.unmodifiableMap(Stream.of(
                    new AbstractMap.SimpleEntry<>("event", "retrieveWallets"),
                    new AbstractMap.SimpleEntry<>("user", userId))
                                                       .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey,
                                                                                 AbstractMap.SimpleEntry::getValue))));

            // TODO: txn id is currently a timestamp
            return walletService.retrieveWallets(headers, Long.toString(System.currentTimeMillis()), userId);
        }
    }

    @GetMapping("/issuer/{issuerId}/find")
    public String findIssuer(@PathVariable String issuerId) {
        LOGGER.info("Find issuer: " + issuerId);
        return registryService.findIssuerMapping(issuerId);
    }

    @PostMapping("/issuer/register")
    public void addIssuer(@RequestParam("issuerId") String issuerId, @RequestParam("issuerAddress") String issuerAddress) {
        LOGGER.info("Add issuer: " + issuerId + " address: " + issuerAddress);
        registryService.addIssuerMapping(issuerId, issuerAddress);
        // clear ALL users from user cache since they may have wallet in new issuer
        userCache.clear();
    }

    @PostMapping("/issuer/deregister/{issuerId}")
    public String removeIssuer(@PathVariable String issuerId) {
        LOGGER.info("Remove  issuer: " + issuerId);
        String issuerAddress = registryService.removeIssuerMapping(issuerId);
        // remove all instances of issuer address from user cache
        userCache.clearIssuer(issuerAddress);
        return issuerAddress;
    }

    public static ActiveSpan startSpan(Tracer tracer, Map<String, String> headers, String spanKind, String operationName) {

        io.opentracing.Tracer.SpanBuilder spanBuilder;
        try {
            SpanContext parentSpanCtx = tracer.extract(Format.Builtin.HTTP_HEADERS, new TextMapExtractAdapter(headers));
            if (parentSpanCtx == null) {
                LOGGER.debug("Span parent context is null. Starting new.");
                spanBuilder = tracer.buildSpan(operationName);
            } else {
                LOGGER.debug("Span parent context: " + parentSpanCtx.toString());
                spanBuilder = tracer.buildSpan(operationName).asChildOf(parentSpanCtx);
            }
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Yow! Starting new: "+e.getLocalizedMessage());
            spanBuilder = tracer.buildSpan(operationName);
        }
        // TODO could add more tags like http.url
        return spanBuilder.withTag(Tags.SPAN_KIND.getKey(), spanKind).startActive();
    }

}
