package com.objectcomputing.monstermesh.model;

public class Wallet {

    private String issuer_id;
    private String last_name;
    private String first_name;
    private String payment_token;
    private String street_address;
    private String city;
    private String state_province;
    private String postal_code;
    private String country_name;
    private String icon;

    public Wallet() {/* required for serialization */}

    public Wallet(String issuer_id,
                  String last_name,
                  String first_name,
                  String payment_token,
                  String street_address,
                  String city,
                  String state_province,
                  String postal_code,
                  String country_name,
                  String icon) {
        this.issuer_id = issuer_id;
        this.last_name = last_name;
        this.first_name = first_name;
        this.payment_token = payment_token;
        this.street_address = street_address;
        this.city = city;
        this.state_province = state_province;
        this.postal_code = postal_code;
        this.country_name = country_name;
        this.icon = icon;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPayment_token() {
        return payment_token;
    }

    public void setPayment_token(String payment_token) {
        this.payment_token = payment_token;
    }

    public String getStreet_address() {
        return street_address;
    }

    public String getIssuer_id() {
        return issuer_id;
    }

    public void setIssuer_id(String issuer_id) {
        this.issuer_id = issuer_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet_address(String street_address) {
        this.street_address = street_address;
    }

    public String getState_province() {
        return state_province;
    }

    public void setState_province(String state_province) {
        this.state_province = state_province;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "issuer_id='" + issuer_id + '\'' +
                ", last_name='" + last_name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", payment_token='" + payment_token + '\'' +
                ", street_address='" + street_address + '\'' +
                ", city='" + city + '\'' +
                ", state_province='" + state_province + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", country_name='" + country_name + '\'' +
                ", icon='" + icon + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wallet wallet = (Wallet) o;

        if (!issuer_id.equals(wallet.issuer_id)) return false;
        if (!last_name.equals(wallet.last_name)) return false;
        if (!first_name.equals(wallet.first_name)) return false;
        if (!payment_token.equals(wallet.payment_token)) return false;
        if (!street_address.equals(wallet.street_address)) return false;
        if (!city.equals(wallet.city)) return false;
        if (!state_province.equals(wallet.state_province)) return false;
        if (!postal_code.equals(wallet.postal_code)) return false;
        if (!country_name.equals(wallet.country_name)) return false;
        return icon.equals(wallet.icon);
    }

    @Override
    public int hashCode() {
        int result = issuer_id.hashCode();
        result = 31 * result + last_name.hashCode();
        result = 31 * result + first_name.hashCode();
        result = 31 * result + payment_token.hashCode();
        result = 31 * result + street_address.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + state_province.hashCode();
        result = 31 * result + postal_code.hashCode();
        result = 31 * result + country_name.hashCode();
        result = 31 * result + icon.hashCode();
        return result;
    }
}
