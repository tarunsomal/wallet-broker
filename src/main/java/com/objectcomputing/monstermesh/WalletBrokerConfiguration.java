package com.objectcomputing.monstermesh;

import com.objectcomputing.monstermesh.registry.IssuerRegistry;
import com.objectcomputing.monstermesh.registry.IssuerRegistryRedis;
import com.objectcomputing.monstermesh.registry.UserCache;
import com.objectcomputing.monstermesh.registry.UserCacheRedis;
import com.uber.jaeger.Configuration;
import com.uber.jaeger.Tracer;
import com.uber.jaeger.samplers.ConstSampler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@org.springframework.context.annotation.Configuration
@ComponentScan("com.objectcomputing.monstermesh")
public class WalletBrokerConfiguration extends WebMvcConfigurerAdapter {
    // TODO: make all log.info() log.debug()
    private static final Logger LOGGER = LoggerFactory.getLogger(WalletBrokerConfiguration.class);

    @Value("${monstermesh.redis.container.name}")
    private String redisNetwork;

    @Value("${monstermesh.redis.port}")
    private int redisPort;

    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        checkEnvironment();
        JedisConnectionFactory connFactory = new JedisConnectionFactory();
        // As long as we've created a common Docker network and started our containers in it,
        // we refer to the Redis docker container name as the hostname and let Docker's DNS resolve
        connFactory.setHostName(redisNetwork);
        connFactory.setPort(redisPort);
        connFactory.afterPropertiesSet();
        return connFactory;
    }

    private void checkEnvironment() {
        String envRedisNetwork = System.getenv("REDIS_NETWORK");
        if (envRedisNetwork != null) {
            LOGGER.info("Overriding redisNetwork with env var: " + envRedisNetwork);
            redisNetwork = envRedisNetwork;
        }

        String envRedisPortStr = System.getenv("REDIS_PORT");
        if (envRedisPortStr != null) {
            LOGGER.info("Overriding redisPort with env var: " + envRedisPortStr);
            redisPort = Integer.parseInt(envRedisPortStr);
        }
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public RedisTemplate<String, List<String>> redisUserTemplate() {
        final RedisTemplate<String, List<String>> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setValueSerializer(new GenericToStringSerializer<>(List.class));
        return template;
    }

    @Bean
    public RedisTemplate<String, String> redisIssuerTemplate() {
        final RedisTemplate<String, String> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        template.setValueSerializer(new GenericToStringSerializer<>(String.class));
        return template;
    }

    @Bean
    IssuerRegistry redisIssuerCache() {
        return new IssuerRegistryRedis(redisIssuerTemplate());
    }

    @Bean
    UserCache redisUserCache() {
        return new UserCacheRedis(redisUserTemplate());
    }

    @Bean
    public Tracer jaegerTracer() {
        Configuration.SamplerConfiguration samplerConfig = new Configuration.SamplerConfiguration(ConstSampler.TYPE, 1);
        Configuration.ReporterConfiguration reporterConfig = new Configuration.ReporterConfiguration(true,
                                                                                                     null,
                                                                                                     null,
                                                                                                     null,
                                                                                                     null);
        Configuration config = new Configuration("wallet-broker", samplerConfig, reporterConfig);
        return (Tracer) config.getTracer();
    }
}
