package com.objectcomputing.monstermesh.registry;

import java.util.Map;

public interface IssuerRegistry {

    String findIssuer(String id);

    Map<String, String> getIssuers();

    void addIssuer(String id, String address);

    String removeIssuer(String id);

    void clear();
}
