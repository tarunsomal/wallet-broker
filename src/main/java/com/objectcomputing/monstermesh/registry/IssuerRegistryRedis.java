package com.objectcomputing.monstermesh.registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Map;

public class IssuerRegistryRedis implements IssuerRegistry {
    // TODO: make all log.info() log.debug()
    private static final Logger LOGGER = LoggerFactory.getLogger(IssuerRegistryRedis.class);
    private static final String ISSUER_CACHE = "monstermesh/issuer";

    // RedisTemplate<issuerID, issuerAddress>
    private RedisTemplate<String, String> template;
    private HashOperations<String, String, String> hashOperations;

    @Autowired
    public IssuerRegistryRedis(RedisTemplate<String, String> template) {
        this.template = template;
    }

    @PostConstruct
    private void init() {
        hashOperations = template.opsForHash();
    }

    @Override
    public String findIssuer(String issuerId) {
        LOGGER.info("Finding Issuer: " + issuerId);
        String address = hashOperations.get(ISSUER_CACHE, issuerId);
        if (address != null) {
            LOGGER.info("Found Issuer: " + issuerId + " at: " + address);
        } else {
            LOGGER.info("Did not find Issuer: " + issuerId);
        }
        return address;
    }

    @Override
    public Map<String, String> getIssuers() {
        LOGGER.info("Getting all Issuers");
        Map<String, String> issuers = hashOperations.entries(ISSUER_CACHE);
        if (issuers == null || issuers.isEmpty()) {
            LOGGER.error("No issuers in registry.");
        }
        return issuers;
    }

    @Override
    public void addIssuer(String issuerId, String address) {
        LOGGER.info("Adding Issuer: " + issuerId + " at: " + address);
        hashOperations.put(ISSUER_CACHE, issuerId, address);
    }

    @Override
    public String removeIssuer(String issuerId) {
        String address = hashOperations.get(ISSUER_CACHE, issuerId);
        if (address != null) {
            LOGGER.info("Removing Issuer: " + issuerId);
            hashOperations.delete(ISSUER_CACHE, issuerId);
        } else {
            LOGGER.info("No Issuer: " + issuerId + " in cache to remove.");
        }
        return address;
    }

    @Override
    public void clear() {
        LOGGER.info("Clearing issuer registry");
        Collection<String> keys = template.keys(ISSUER_CACHE);
        if (keys != null && ! keys.isEmpty()) {
            LOGGER.info("Clearing these keys from issuer registry: " + keys);
            template.delete(keys);
        } else {
            LOGGER.info("No active keys in issuer registry.");
        }
    }

}
