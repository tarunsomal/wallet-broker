package com.objectcomputing.monstermesh.registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

@Configuration
@Profile("production")
public class IssuerConfig {
    // TODO: make all log.info() log.debug()
    private static final Logger LOGGER = LoggerFactory.getLogger(IssuerConfig.class);

    public static final String TXN_ID_PAT = "<txnId>";
    public static final String UID_PAT = "<UID>";

    @Autowired
    private IssuerRegistry issuerRegistry;
    @Autowired
    private UserCache userCache;

    private ConfigMapWatcher watcher;

    @PostConstruct
    private void init() {
        Path configFile = FileSystems.getDefault().getPath("/deployments/config/issuers");
        // verify we can read config map
        checkFileIsReadable(configFile);

        // clear user cache - we might be redeploying
        userCache.clear();
        // clear issuer registry - we might be redeploying
        issuerRegistry.clear();

        // read config file and monitor for changes
        monitorConfigMap(configFile);
    }

    /**
     * Create a file watcher thread for the config map volume
     * @param configFile file path that contains our config.
     */
    protected void monitorConfigMap(Path configFile) {
        watcher = new ConfigMapWatcher(configFile,
                                       new ConfigModifier(new IssuerAdder(issuerRegistry, userCache),
                                                          new IssuerRemover(issuerRegistry, userCache),
                                                          new ReaderFunction()));
        new Thread(watcher).start();
    }

    /**
     * Check to make sure we can read given file.
     * @param configFile file path that contains our config.
     */
    protected void checkFileIsReadable(Path configFile) {
        // TODO: remove all debug logging...
        if (! Files.exists(configFile)) {
            LOGGER.error("Config file does not exist: " + configFile);
            if (! Files.exists(configFile.getParent())) {
                LOGGER.error("Config dir does not exist: " + configFile.getParent());
                if (! Files.exists(configFile.getParent().getParent())) {
                    LOGGER.error("Config root dir does not exist: " + configFile.getParent().getParent());
                } else {
                    LOGGER.error("Config root dir DOES exist: " + configFile.getParent().getParent());
                }
            } else {
                LOGGER.error("Config dir DOES exist: " + configFile.getParent());
                try {
                    Files.list(configFile.getParent()).forEach(f -> LOGGER.info("  "+f.toString()));
                } catch (IOException e) {
                    LOGGER.error("Cannot get file listing: "+e.getLocalizedMessage());
                }
            }
            throw new RuntimeException("Config file does not exist: " + configFile);
        } else if (! Files.isReadable(configFile)) {
            LOGGER.error("Config file exists but is not readable: " + configFile);
        } else {
            try {
                LOGGER.info("Config file contents: "+Files.readAllLines(configFile));
            } catch (IOException e) {
                LOGGER.error("Config file read failed: "+ e.getLocalizedMessage());
                throw new RuntimeException("Config file cannot be read: " + configFile);
            }
        }
    }

    static class ReaderFunction implements Function<FileReader, List<String>> {

        @Override
        public List<String> apply(FileReader fileReader) {
            LOGGER.info("Parsing config file: "+fileReader);
            List<String> list = new ArrayList<>();
            Yaml yaml = new Yaml();
            String issuerUris = yaml.load(fileReader);
            if (issuerUris != null && ! issuerUris.isEmpty()) {
                list.addAll(Arrays.asList(issuerUris.split("\\s")));
            }
            LOGGER.info("Parsed config file. Got: "+list);
            return list;
        }
    }

    /**
     * Add new issuers added to the config, clearing caches as appropriate.
     */
    static final class IssuerAdder implements Consumer<String> {
        final IssuerRegistry issuerRegistry;
        final UserCache userCache;

        IssuerAdder(IssuerRegistry issuerRegistry, UserCache userCache) {
            this.issuerRegistry = issuerRegistry;
            this.userCache = userCache;
        }
        @Override
        public void accept(String issuerAddress) {
            String issuerId = getIssuerName(issuerAddress);
            LOGGER.info("Add issuer: " + issuerId + " address: " + issuerAddress);
            issuerRegistry.addIssuer(issuerId, issuerAddress);
            // clear ALL users from user cache since they may have wallet in new issuer
            userCache.clear();
        }
    }

    /**
     * Remove issuers deleted from the config, clearing caches as appropriate.
     */
    static final class IssuerRemover implements Consumer<String> {
        final IssuerRegistry issuerRegistry;
        final UserCache userCache;

        IssuerRemover(IssuerRegistry issuerRegistry, UserCache userCache) {
            this.issuerRegistry = issuerRegistry;
            this.userCache = userCache;
        }

        @Override
        public void accept(String issuerAddress) {
            String issuerId = getIssuerName(issuerAddress);
            LOGGER.info("Remove issuer: " + issuerId + " address: " + issuerAddress);
            String removedIssuerAddress = issuerRegistry.removeIssuer(issuerId);
            // remove all instances of issuer address from user cache
            userCache.clearIssuer(removedIssuerAddress);
        }
    }

    /**
     * Coordinate modifying the config by determining the issuers that have changed and
     * delegating to an "Adder" to add new issuers or to a "Remover" to delete removed issuers.
     */
    static final class ConfigModifier {
        final Consumer<String> adder;
        final Consumer<String> remover;
        final Function<FileReader, List<String>> configReader;
        Set<String> issuerSet;

        ConfigModifier(Consumer<String> adder,
                       Consumer<String> remover,
                       Function<FileReader, List<String>> configReader) {
            this.adder = adder;
            this.remover = remover;
            this.configReader = configReader;
            this.issuerSet = new HashSet<>();
        }

        void modifyConfig(FileReader yamlFile) {
            issuerSet = issuersChanged(adder, remover, issuerSet, new HashSet<>(configReader.apply(yamlFile)));
        }

        static Set<String> issuersChanged(Consumer<String> adder,
                                   Consumer<String> remover,
                                   Set<String> oldIssuers,
                                   Set<String> newIssuers) {
            LOGGER.info("ConfigModifier - Current issuers: " + oldIssuers);
            LOGGER.info("ConfigModifier - Changed issuers: " + newIssuers);

            Set<String> fini = Stream.concat(oldIssuers.stream(), newIssuers.stream())
                                     .collect(Collectors.toSet());

            for (String toRemove : oldIssuers) {
                if (! newIssuers.contains(toRemove)) {
                    remover.accept(toRemove);
                    fini.remove(toRemove);
                }
            }

            for (String toAdd : newIssuers) {
                if (! oldIssuers.contains(toAdd)) {
                    adder.accept(toAdd);
                }
            }
            LOGGER.info("New issuer set: " + fini);

            return fini;
        }
    }

    /**
     * Watch for changes in a ConfigMap file and, is so, notify the {@link ConfigModifier}
     */
    static final class ConfigMapWatcher implements Runnable {

        private final Path watchedFile;
        private final ConfigModifier configModifier;

        private ConfigMapWatcher(Path watchedFile, ConfigModifier configModifier) {
            this.watchedFile = watchedFile;
            this.configModifier = configModifier;
        }

        public void run() {
            LOGGER.info("ConfigMap dir watch path: " + watchedFile.getParent());

            try (final WatchService watchService = FileSystems.getDefault().newWatchService()) {
                // read the initial load of config
                configModifier.modifyConfig(new FileReader(watchedFile.toFile()));

                // watching file's dir (parent)
                watchedFile.getParent().register(watchService,
                                                 StandardWatchEventKinds.ENTRY_MODIFY,
                                                 StandardWatchEventKinds.ENTRY_CREATE);
                while (true) {
                    final WatchKey wk;
                    try {
                        LOGGER.info("Watching ConfigMap...");
                        wk = watchService.take();
                    } catch (InterruptedException e) {
                    LOGGER.error("ConfigMap watcher got interrupted. Quitting.");
                        return;
                    }

                    for (WatchEvent<?> event : wk.pollEvents()) {
                        if (event.kind() == OVERFLOW) {
                            // event lost, continue
                            continue;
                        }

                        final Path changed = (Path) event.context();
                        LOGGER.info("File changed: [" + changed+"]");
                        if (changed.endsWith("..data")) {
                            // oddly enough, the file that indicates a change is named "..data", not "issuers"
                            // "issuers" file contains the changes but never shows it's changed
                            LOGGER.info("ConfigMap has changed: " + watchedFile);
                            configModifier.modifyConfig(new FileReader(watchedFile.toFile()));
                        }
                    }

                    // reset the key
                    boolean valid = wk.reset();
                    if (! valid) {
                        LOGGER.warn("Config watcher directory key has been unregistered.");
                        break;
                    }
                }
            } catch (IOException e) {
                LOGGER.warn("Config watcher: " + e.getLocalizedMessage());
            }
        }
    }

    static String getIssuerName(String uri) {
        if (uri != null) {
            try {
                int hostStart = (uri.startsWith("https") ? "https://".length() : "http://".length());
                return uri.substring(hostStart, uri.indexOf(':', hostStart));
            } catch (Exception e) {
                LOGGER.warn("Cannot parse Issuer URI: " + uri);
            }
        }
        return null;
    }
}
