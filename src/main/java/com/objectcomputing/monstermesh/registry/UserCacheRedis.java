package com.objectcomputing.monstermesh.registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;
import java.util.*;

public class UserCacheRedis implements UserCache {
    // TODO: make all log.info() log.debug()
    private static final Logger LOGGER = LoggerFactory.getLogger(UserCacheRedis.class);
    private static final String USER_CACHE = "monstermesh/users";

    // RedisTemplate<userId, List<issuerAddress>>
    private RedisTemplate<String, List<String>> template;
    private HashOperations<String, String, List<String>> hashOperations;

    @Autowired
    public UserCacheRedis(RedisTemplate<String, List<String>> template) {
        this.template = template;
    }

    @PostConstruct
    private void init() {
        hashOperations = template.opsForHash();
    }

    @Override
    public List<String> getIssuersForUser(String userId) {
        LOGGER.info("Checking for user '" + userId + "' in cache");
        List<String> addresses = hashOperations.get(USER_CACHE, userId);
        if (addresses != null && ! addresses.isEmpty()) {
            LOGGER.info("User '" + userId + "' cached.");
        } else {
            LOGGER.info("User '" + userId + "' NOT cached.");
            addresses = Collections.emptyList();
        }
        return addresses;
    }

    @Override
    public void cacheUserIssuer(String userId, String issuerAddress) {
        LOGGER.info("Caching user '" + userId + "' at issuer address: " + issuerAddress);
        List<String> addresses = hashOperations.get(USER_CACHE, userId);
        if (addresses == null) {
            addresses = new ArrayList<>();
        }
        if (addresses.isEmpty() || ! addresses.contains(issuerAddress)) {
            addresses.add(issuerAddress);
            LOGGER.info("Cached user '" + userId + "' issuer addresses now: " + addresses);
            hashOperations.put(USER_CACHE, userId, addresses);
        }
    }

    @Override
    public boolean removeIssuerFromUser(String userId, String issuerAddress) {
        LOGGER.info("Removing cached user '" + userId + "' issuer address: " + issuerAddress);
        List<String> addresses = hashOperations.get(USER_CACHE, userId);
        return removeIssuerFromUser(userId, issuerAddress, addresses, hashOperations);
    }

    @Override
    public boolean removeUser(String userId) {
        LOGGER.info("Removing cached user '" + userId + "'");
        List<String> addresses = hashOperations.get(USER_CACHE, userId);
        if (addresses != null) {
            hashOperations.delete(USER_CACHE, userId);
            LOGGER.info("Removed cached user '" + userId + "'");
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        LOGGER.info("Clearing user cache");
        Collection<String> keys = template.keys(USER_CACHE);
        if (keys != null && ! keys.isEmpty()) {
            LOGGER.info("Clearing these keys from user cache: " + keys);
            template.delete(keys);
        } else {
            LOGGER.info("No active keys in user cache.");
        }
    }

    @Override
    public void clearIssuer(String issuerAddress) {
        LOGGER.info("Removing all instances of issuer: " + issuerAddress);
        // TODO: this would be better handled by keeping a reverse cache of issuerAddress -> List<userId>
        Map<String, List<String>> allUsers = hashOperations.entries(USER_CACHE);
        if (allUsers != null) {
            for(Map.Entry<String, List<String>> users : allUsers.entrySet() ) {
                LOGGER.info("Checking user '" + users.getKey() + "' for address: " + issuerAddress);
                removeIssuerFromUser(users.getKey(), issuerAddress, users.getValue(), hashOperations);
            }
        }
    }

    private static boolean removeIssuerFromUser(String userId,
                                                String issuerAddress,
                                                List<String> addresses,
                                                HashOperations<String, String, List<String>> hashOperations) {
        if (addresses != null && addresses.remove(issuerAddress)) {
            LOGGER.info("Removed issuer address '" + issuerAddress + "' from cached user '" + userId + "'");
            LOGGER.info("User '" + userId + "' now has cached issuer address: " + addresses);
            if (addresses.isEmpty()) {
                hashOperations.delete(USER_CACHE, userId);
            } else {
                hashOperations.put(USER_CACHE, userId, addresses);
            }
            return true;
        }
        return false;
    }
}
