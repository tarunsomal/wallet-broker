package com.objectcomputing.monstermesh;

import com.objectcomputing.monstermesh.controller.WalletBrokerController;
import com.objectcomputing.monstermesh.model.Wallet;
import com.objectcomputing.monstermesh.model.Wallets;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
//@WebMvcTest(WalletBrokerController.class)
public class WalletBrokerTest {

    private static final String ISSUER_ID = "myFakeIssuer";

//    @MockBean
//    private WalletBrokerService walletService;
//    @MockBean
//    private IssuerRegistryService registryService;

    @Mock
    private WalletBrokerController controller;

//    @Autowired
//    private MockMvc mockMvc;

    @Test
    public void getWalletsForUser() throws Exception {
        Wallets expected = createWallets("9999","19");
        Mockito.when(controller.retrieveUserWallets(new HashMap<>(), "19")).thenReturn(expected);

        Wallets wallets = controller.retrieveUserWallets(new HashMap<>(),"19");

//        System.out.println(" * " + wallets);
        assertEquals(expected, wallets);
    }

    private Wallets createWallets(String txnId, String userId) {
        // TODO: does it make sense to have diff address for each issuer (card)?
        Collection<Wallet> wallets = new ArrayList<>();
        wallets.add(new Wallet(ISSUER_ID,
                               "Jones",
                               "Fred",
                               "12345",
                               "13 Rue De La Banc",
                               "Paris",
                               "Île-de-France",
                               "2233",
                               "France",
                               ISSUER_ID + "card2Icon"));
        wallets.add(new Wallet(ISSUER_ID,
                               "Jones",
                               "Alice",
                               "938475",
                               "13 Red Street",
                               "Austin",
                               "Texas",
                               "87123",
                               "United States",
                               ISSUER_ID + "card1Icon"));

        return new Wallets(txnId,userId, wallets);
    }
}
