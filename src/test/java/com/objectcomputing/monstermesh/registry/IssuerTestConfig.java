package com.objectcomputing.monstermesh.registry;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.nio.file.Path;

@Configuration
@Profile("test")
public class IssuerTestConfig extends IssuerConfig {

    @Override
    protected void checkFileIsReadable(Path configFile) {
        // do nothing for test profile
    }

    @Override
    protected void monitorConfigMap(Path configFile) {
        // do nothing for test profile
    }
}
