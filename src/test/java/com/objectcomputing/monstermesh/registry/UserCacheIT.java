package com.objectcomputing.monstermesh.registry;

import com.objectcomputing.monstermesh.WalletBrokerConfiguration;
import com.objectcomputing.monstermesh.controller.WalletBrokerController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalletBrokerConfiguration.class)
public class UserCacheIT {
    private static final String FRED = "fred";
    private static final String JOE = "joe";
    private static final String WILMA = "wilma";

    @Before
    public void beforeClass() throws Exception {
        userCache.removeUser(FRED);
        userCache.removeUser(JOE);
        userCache.removeUser(WILMA);
    }

    @Autowired
    private UserCache userCache;

    @Autowired
    private WalletBrokerController controller;

    @Test
    public void cacheUser() throws Exception {
        String address = "https://localhost:1313/FrankinIssuer";

        userCache.cacheUserIssuer(FRED, address);
        assertEquals(Collections.singletonList(address), userCache.getIssuersForUser(FRED));
    }

    @Test
    public void removeUser() throws Exception {
        String address = "https://localhost:4545/RubleWay";

        userCache.cacheUserIssuer(WILMA, address);
        assertEquals(Collections.singletonList(address), userCache.getIssuersForUser(WILMA));

        assertTrue(userCache.removeUser(WILMA));
        assertEquals(Collections.emptyList(), userCache.getIssuersForUser(WILMA));
    }

    @Test
    public void cacheUserDeleteAddress() throws Exception {
        String address1 = "https://localhost:1414/DinoIssuer";
        String address2 = "https://localhost:1313/FrankinIssuer";

        userCache.cacheUserIssuer(JOE, address1);
        userCache.cacheUserIssuer(JOE, address2);
        assertEquals(Arrays.asList(address1, address2), userCache.getIssuersForUser(JOE));

        assertTrue(userCache.removeIssuerFromUser(JOE, address1));
        assertEquals(Collections.singletonList(address2), userCache.getIssuersForUser(JOE));
    }

    @Test
    public void removeIssuer() throws Exception {
        String address1 = "https://localhost:1414/DinoIssuer";
        String address2 = "https://localhost:1313/FrankinIssuer";
        String address3 = "https://localhost:4545/RubleWay";

        userCache.cacheUserIssuer(JOE, address1);
        userCache.cacheUserIssuer(JOE, address2);
        assertEquals(Arrays.asList(address1, address2), userCache.getIssuersForUser(JOE));

        userCache.cacheUserIssuer(WILMA, address1);
        userCache.cacheUserIssuer(WILMA, address3);
        assertEquals(Arrays.asList(address1, address3), userCache.getIssuersForUser(WILMA));

        userCache.clearIssuer(address1);
        assertEquals(Collections.singletonList(address2), userCache.getIssuersForUser(JOE));
        assertEquals(Collections.singletonList(address3), userCache.getIssuersForUser(WILMA));
    }

    @Test
    public void testClear() throws Exception {
        String address1 = "https://localhost:1414/DinoIssuer";
        String address2 = "https://localhost:1313/FrankinIssuer";
        String address3 = "https://localhost:4545/RubleWay";

        userCache.cacheUserIssuer(JOE, address1);
        userCache.cacheUserIssuer(JOE, address2);
        userCache.cacheUserIssuer(JOE, address3);
        assertEquals(Arrays.asList(address1, address2, address3), userCache.getIssuersForUser(JOE));

        userCache.cacheUserIssuer(WILMA, address1);
        userCache.cacheUserIssuer(WILMA, address2);
        userCache.cacheUserIssuer(WILMA, address3);
        assertEquals(Arrays.asList(address1, address2, address3), userCache.getIssuersForUser(WILMA));

        userCache.cacheUserIssuer(FRED, address1);
        userCache.cacheUserIssuer(FRED, address2);
        userCache.cacheUserIssuer(FRED, address3);
        assertEquals(Arrays.asList(address1, address2, address3), userCache.getIssuersForUser(FRED));

        userCache.clear();
        assertEquals(Collections.emptyList(), userCache.getIssuersForUser(JOE));
        assertEquals(Collections.emptyList(), userCache.getIssuersForUser(WILMA));
        assertEquals(Collections.emptyList(), userCache.getIssuersForUser(FRED));
    }
}
