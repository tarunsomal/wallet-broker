package com.objectcomputing.monstermesh.registry;

import com.objectcomputing.monstermesh.WalletBrokerConfiguration;
import com.objectcomputing.monstermesh.controller.WalletBrokerController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalletBrokerConfiguration.class)
public class IssuerRegistryIT {

    @Autowired
    private IssuerRegistry registry;

    @Autowired
    private WalletBrokerController controller;

    @Test
    public void roundTripIssuers() throws Exception {
        String id = "13";
        String address = "https://localhost:1313/FrankinIssuer";

        registry.addIssuer(id, address);
        assertEquals(address, registry.findIssuer(id));
    }

    @Test
    public void roundTripIssuerController() throws Exception {
        String id = "13";
        String address = "https://localhost:1313/FrankinIssuer";

        controller.addIssuer(id, address);
        assertEquals(address, controller.removeIssuer(id));
        assertNull(registry.findIssuer(id));
    }

}
