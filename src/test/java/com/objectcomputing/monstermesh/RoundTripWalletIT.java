package com.objectcomputing.monstermesh;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static com.objectcomputing.monstermesh.registry.IssuerConfig.TXN_ID_PAT;
import static com.objectcomputing.monstermesh.registry.IssuerConfig.UID_PAT;
import static com.objectcomputing.monstermesh.WebFrontIssuerRegistryIT.buildPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WalletBroker.class)
@WebAppConfiguration
public class RoundTripWalletIT {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetWallets() throws Exception {
        // issuer details - id/address
        String issuerId = "dingle";
        String issuerAddress = buildPath("http://localhost:5001/v1/wallets/transaction_id",
                                         TXN_ID_PAT,
                                         "uid",
                                         UID_PAT);

        // first, register new issuer with broker service
        mockMvc.perform(post("/broker/issuer/register")
                                .param("issuerId", issuerId)
                                .param("issuerAddress", issuerAddress))
               .andExpect(status().isOk())
               .andReturn();

        // second, make sure register succeeded
        mockMvc.perform(get(buildPath("/broker/issuer", issuerId, "find")))
               .andExpect(status().isOk())
               .andReturn();

        // third, request wallets for user 999
        MvcResult result = mockMvc.perform(get("/broker/999/wallets"))
                                  .andExpect(status().isOk())
                                  .andReturn();
        System.out.println(result.getResponse().getContentAsString());

        // FIXME: random transaction_id causes assertEquals() failure
//        String expected = "{\"uid\":\"999\",\"transaction_id\":\"1510869921404\",\"wallets\":[{\"issuer_id\":\"fake_issuer\",\"last_name\":\"Jones\",\"first_name\":\"Fred\",\"payment_token\":\"12345\",\"street_address\":\"13 Rue De La Banc\",\"city\":\"Paris\",\"state_province\":\"Île-de-France\",\"postal_code\":\"2233\",\"country_name\":\"France\",\"icon\":\"fake_issuercard2Icon\"},{\"issuer_id\":\"fake_issuer\",\"last_name\":\"Jones\",\"first_name\":\"Alice\",\"payment_token\":\"938475\",\"street_address\":\"13 Red Street\",\"city\":\"Austin\",\"state_province\":\"Texas\",\"postal_code\":\"87123\",\"country_name\":\"United States\",\"icon\":\"fake_issuercard1Icon\"}]}";
//        assertEquals(expected, result.getResponse().getContentAsString());
    }
}
