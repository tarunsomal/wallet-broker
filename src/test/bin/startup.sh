#!/usr/bin/env bash

# Quick and dirty shell script to start all containers in given network and
# register the issuer service in Redis.
# When the script finishes, the UI will be ready to hit.
#
# This script presumes you have built all the docker images already.

# Set the redis network name - set in shutdown.sh too!
export REDIS_NETWORK=monstermeshredis
echo "REDIS_NETWORK set to $REDIS_NETWORK"

echo "create the common network"
docker network create monsternet

echo "run redis container attaching to network"
docker run --net=monsternet --rm -p 6379:6379 --name $REDIS_NETWORK -d redis

echo "Remove any old, persisted hashes"
redis-cli FLUSHALL

echo "Start the issuer service"
docker run --net=monsternet --rm -p 8080:8080 -p 9090:9090 --name issuer_ep -d issuer

echo "start the wallet broker service"
docker run --net=monsternet --rm -p 5000:8080 --name wallet_broker --env REDIS_NETWORK=$REDIS_NETWORK -d monstermesh/wallet-broker

echo "Wait for Spring to start up..."
sleep 5

# Register the issuer service (id -> endpoint address) with Redis so that wallet broker can talk to it
#curl -i -d "issuerId=issuer1&issuerAddress=http://issuer_ep:8080/v1/wallets/transaction_id/<txnId>/uid/<UID>" -X POST http://localhost:5000/broker/issuer/register

echo "Test that we can get wallets from wallet broker (hits Redis for issuer address and make request"
echo " from issuer to get the wallets for the given user id"
curl -X GET -k http://localhost:5000/broker/johndoe@gmail.com/wallets

echo
echo "If we got JSON, then server is up! Starting UI..."

echo "Start the UI container"
docker run --net=monsternet --name=wallet_ui --rm -p 3000:3000 -d wallet/ui

echo "Point browser to http://localhost:3000"

