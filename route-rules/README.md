## Routing and Versioning Recommendations for Blue/Green Deployment
See [Istio Route Rules](https://istio.io/docs/reference/config/traffic-rules/routing-rules.html)

The scenario is that we have a running, production version of wallet-broker in our
*monstermesh* application that queries issuers *serially* for a user's wallets.  We've
seen a lot of new issuers come on board since we've released that first version of
*monstermesh* and, as the number of issuers has increased, we've noticed increased
latency when requesting users' wallets.  So now we've decided to implement performance
improvements to wallet-broker, such as caching issuers for a user and enabling
wallet-broker to query issuers asynchronously for a given user's wallets.  This
scenario describes the implementation and deployment of the *parallel* fetch version
of wallet-broker.

Using good software development practices, we create a *branch* to do our work to
enable wallet-broker to query issuers in parallel.  We'll call this branch *parallel*.
When we're finished with implementation and testing, we can deploy this new version
running simultaneously in our cluster using Istio route rules.  We've based our *version*
in our route rule on our *branch name* or *feature name* so that we can see immediately
where we're routing.  Naming the new version with *branch name* can also help us
with CI/CD automation, as integration/deployment tools are cognisant of branches.

The default route rule (`serial` version, which is on the `master` branch) is shown 
[here](../k8s/route-rule-default.yaml).
This is what we have running in the cluster currently.  As you can see, we have 100%
of the traffic going to the default version, as you might expect.  Our new  route rule
for the wallet-broker (`parallel` version) is shown [here](./route-rule-v2.yaml).
Since this is a Blue/Green Deployment scenario, we will be *replacing* the default
wallet-broker route rule and divert 100% of the traffic to the `parallel` version.  In
other deployment scenarios we might only divert a small percentage of traffic to our
new version and increase slowly as we build confidence in our new service.

Note that we're assuming, for demonstration purposes, that we've already done significant
testing and verification of our new version so that we have high confidence that it will
not fail catastrophically.  However, if some unforeseen failure did occur, the `serial`
version is *still* running in our cluster.  All we would need to do is again replace
the new route rule with the old and the *monstermesh* application would be running
with the old, `serial` version immediately without restarting.  This is done manually
for this demonstration but could be automated based on feedback from Istio failure
metrics.

Note also that we've glossed over the *testing* stage of our development.  This can
also be facilitated using Istio route rules in a *Canary* deployment scenario.  That
may be implemented by giving the `spec.route.weight` a value of, say, 10% to begin
with and gradually increase as confidence in the new version increases.  That can, of
course, also be automated based on feedback from Istio for the new version.

For a good overview of deployment strategies, see
[Kubernetes Deployment Strategies](http://container-solutions.com/kubernetes-deployment-strategies/).

## Steps
Assume we have two development branches; `master` (the `serial` version that's running
currently) and the new version, `parallel`, that we've developed and tested on the
`parallel` branch.

In our `parallel` branch, we tag our Docker image (`parallel`) and push it to the
container registry:

```
$ docker tag monstermesh/wallet-broker:latest gcr.io/mc-digital-native/wallet-broker:parallel
$ gcloud docker -- push gcr.io/mc-digital-native/wallet-broker
```

Now we have both wallet-broker container images in the container registry; one tagged
`master` (the current `serial` version) and the new version tagged `parallel`.

We've previously created our default Istio route rule for the current version with:

```
$ istioctl create -f k8s/route-rule-default.yaml
```

which we can verify by:

```
$ istioctl get routerule wallet-broker -o yaml -n staging
```

	apiVersion: config.istio.io/v1alpha2
	kind: RouteRule
	metadata:
	  annotations:
	    kubectl.kubernetes.io/last-applied-configuration: |
	      {"apiVersion":"config.istio.io/v1alpha2","kind":"RouteRule","metadata":{"annotations":{},"name":"wallet-broker","namespace":"staging"},"spec":{"destination":{"name":"wallet-broker","namespace":"staging"},"precedence":1,"route":[{"labels":{"version":"master"},"weight":100}]}}
	  creationTimestamp: null
	  name: wallet-broker
	  namespace: staging
	  resourceVersion: "1651043"
	spec:
	  destination:
	    name: wallet-broker
	    namespace: staging
	  precedence: 1
	  route:
	  - labels:
	      version: master
	    weight: 100
	---


Now we can deploy our new `parallel` version of wallet-broker specifying the
`parallel` image container:

```
$ cat k8s/deployment.yaml | sigil 'image_tag=gcr.io/mc-digital-native/wallet-broker:parallel' | kubectl apply -f -
```

We verify that *both* `master` and `parallel` wallet-broker pods are running:

```
$ kubectl get pods
```

	NAME                                      READY     STATUS    RESTARTS   AGE
	issuer1-deployment-78455d67d8-pg8rn       2/2       Running   0          4h
	issuer2-deployment-647858db9b-bfgn2       2/2       Running   0          4h
	issuer3-deployment-6784f4b888-vgxx5       2/2       Running   0          4h
	redis-678b98d4c7-q6zvc                    2/2       Running   0          4d
	wallet-broker-parallel-67794568fc-nz4br   2/2       Running   0          47m
	wallet-broker-master-8575bf5b75-fwb4d     2/2       Running   0          1h
	wallet-ui-874dc4f78-m9ds6                 2/2       Running   0          2d

Now we *replace* the default route rule with route rule for the `parallel` version of
wallet-broker and verify:

```
$ istioctl replace -f route-rules/route-rule-v2.yaml
```

	Updated config route-rule/staging/wallet-broker to revision 1660921

```
$ istioctl get routerule wallet-broker -n staging -o yaml
```

	apiVersion: config.istio.io/v1alpha2
	kind: RouteRule
	metadata:
	  creationTimestamp: null
	  name: wallet-broker
	  namespace: staging
	  resourceVersion: "1660921"
	spec:
	  destination:
	    name: wallet-broker
	    namespace: staging
	  precedence: 1
	  route:
	  - labels:
	      version: parallel
	    weight: 100
	---

When we view the call trace in Jaeger, we notice that we're now invoking the `parallel`
version of wallet-broker.

Now imagine we see some some errors that we missed in testing.  We can flip right
back to the "tried-and-true" wallet-broker version.

We again *replace* the new route config with the old,

```
$ istioctl replace -f k8s/route-rule-default.yaml
```

and we're immediately routed back to the old version without restarting.
